package stepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SearchFeatureStepDefinition {
	
	public static WebDriver driver;

	@Given("^I am in home page$")
	public void i_am_in_home_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:/Users/nagar/OneDrive/Desktop/Automation/Driver/chromedriver.exe");
	    driver=new ChromeDriver();
	    driver.get("https://tuclothing.sainsburys.co.uk/");

	}

	@When("^I search for a valid product name$")
	public void i_search_for_a_valid_product_name() throws Throwable {
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("Dress");
		driver.findElement(By.cssSelector(".searchButton")).click();
	    
	}

	@Then("^I should see the search results$")
	public void i_should_see_the_search_results() throws Throwable {
	    
	}

	@When("^I search for an invalid product name$")
	public void i_search_for_an_invalid_product_name() throws Throwable {
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("hgvfjrhgk");
		driver.findElement(By.cssSelector(".searchButton")).click();
	   
	}
	
	@When("^I search for \"([^\"]*)\"$")
	public void i_search_for(String searchWord) throws Throwable {
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys(searchWord);
		driver.findElement(By.cssSelector(".button")).click();
		
	} 
	
	@Then("^I should see the no results found page$")
	public void i_should_see_the_no_results_found_page() throws Throwable {
	    
	}

	@Given("^I leave the search textbox empty$")
	public void i_leave_the_search_textbox_empty() throws Throwable {
	   
	}

	@When("^When i click the search button$")
	public void when_i_click_the_search_button() throws Throwable {
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys(" ");
		driver.findElement(By.cssSelector(".searchButton")).click();
	   
	}

	@When("^I search for a valid product code$")
	public void i_search_for_a_valid_product_code() throws Throwable {
	    driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("135477201");
		driver.findElement(By.cssSelector(".searchButton")).click();
	}
	
	@When("^I search for an invalid product code$")
	public void i_search_for_an_invalid_product_code() throws Throwable {
		 driver.findElement(By.cssSelector("#search")).clear();
			driver.findElement(By.cssSelector("#search")).sendKeys("abcr3");
			driver.findElement(By.cssSelector(".searchButton")).click();
	    
	}

	@When("^I search for numbers$")
	public void i_search_for_numbers() throws Throwable {
		 driver.findElement(By.cssSelector("#search")).clear();
			driver.findElement(By.cssSelector("#search")).sendKeys("123456");
			driver.findElement(By.cssSelector(".searchButton")).click();
	    
	}

	@When("^I search for special characters$")
	public void i_search_for_special_characters() throws Throwable {
		 driver.findElement(By.cssSelector("#search")).clear();
			driver.findElement(By.cssSelector("#search")).sendKeys("!@�$%&*");
			driver.findElement(By.cssSelector(".searchButton")).click();
	   
	}

	@When("^I search for all capital valid data$")
	public void i_search_for_all_capital_valid_data() throws Throwable {
		 driver.findElement(By.cssSelector("#search")).clear();
			driver.findElement(By.cssSelector("#search")).sendKeys("JEANS");
			driver.findElement(By.cssSelector(".searchButton")).click();
	}

	@When("^I search for alphanumeric numbers$")
	public void i_search_for_alphanumeric_numbers() throws Throwable {
		 driver.findElement(By.cssSelector("#search")).clear();
			driver.findElement(By.cssSelector("#search")).sendKeys("Krit0508");
			driver.findElement(By.cssSelector(".searchButton")).click();
	   
	}

	@When("^I search with the brand name$")
	public void i_search_with_the_brand_name() throws Throwable {
		 driver.findElement(By.cssSelector("#search")).clear();
			driver.findElement(By.cssSelector("#search")).sendKeys("Mamalicious");
			driver.findElement(By.cssSelector(".searchButton")).click();
	    
	}

	@When("^I search with catagory$")
	public void i_search_with_catagory() throws Throwable {
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("Holiday");
		driver.findElement(By.cssSelector(".searchButton")).click();
	   
	}

	@When("^I search for sub catagory$")
	public void i_search_for_sub_catagory() throws Throwable {
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("New Arrivals");
		driver.findElement(By.cssSelector(".searchButton")).click();
	   
	}


}



