package cucumberrunner;

import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		glue="stepDefinition", 
        features = "src/test/resources/features",
        tags="@krit")
public class Runner {

}
