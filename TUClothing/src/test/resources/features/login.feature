@login
Feature: Login
@krit
Scenario Outline: Login With Valid Data
Given I am in home page
When I click on login link 
And I entered a valid "<username>" and "<password>"
And I click on submit  button
Then I should see the search results

Examples:
|username|password|
|krit@me.com|pass123|

Scenario Outline: Login With InValid Data
Given I am in home page
When I click on login link
And I entered an invalid "<email address>" and "<password>"
And I click on submit  button
Then I should see the error page

Examples:
|username|password|
|krit@me|pass123.com|

Scenario: Login With Empty Data
Given I am in home page
When I entered an empty text
And I click on submit  button
Then I should see the error message

Scenario Outline: Login With Partial Data
Given I am in home page
When I entered a valid "<email address>"
And I leave the password textbox empty"<password>"
And I click on submit  button
Then I should see the error message 

Examples:
|username|password|
|krit@me.com| |

Scenario: Login forgot password
Given I am in home page
When I click on forgotten your password
Then I should see the forgotten password pop up page

