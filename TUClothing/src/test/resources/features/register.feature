@reg
Feature: Register

Scenario: Register With Valid Data
Given I am in home page
And I click on register link
And I click on the register button
When I entered register details
|email address|test@test.com|
|title|mrs|
|first name|kritika|
|last name|nagar|
|Password|pass123|
|confirm password|pass123|
And I click the check boxes
And I click complete registration button
Then I should see the search results

Scenario: Register With InValid Data
Given I am in home page
And I click on register link
And I click on the register button
When I entered register details
|email address|test@test|
|title| |
|first name|vghfbvjhg|
|last name|235435|
|Password|test@test.com|
|confirm password|zdfxfctrtrers|
And I click the check boxes
And I click complete registration button
Then I should see the search results

Scenario: Register With Empty Data
Given I am in home page
And I click on register link
And I click on the register button
When I entered register details
|email address| |
|title| |
|first name| |
|last name| |
|Password| |
|confirm password| |
And I click the check boxes
And I click complete registration button
Then I should see the search results

Scenario: Register With Partial Data
Given I am in home page
And I click on register link
And I click on the register button
When I entered register details
|email address|test@test|
|title| |
|first name| |
|last name|235435|
|Password| |
|confirm password| |
And I click the check boxes
And I click complete registration button
Then I should see the search results


