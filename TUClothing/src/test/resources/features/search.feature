Feature: Search

@smoke @regression @TU-123 @raghu
Scenario Outline: search with valid product name
Given I am in Home Page
When I search for product "<searchword>"
Then I should see the search results

Examples:

|searchword|
|Jeans|
|Shirts|
|Shoes|

@invalid
Scenario: Search with invalid product name
Given I am in home page
When I search for "kjfhgsjdnmv"
Then I should see the no results found page

@kritzi
Scenario: Search with empty field
Given I am in home page
When I leave the search textbox empty
And When i click the search button
Then I should see the no results found page

@kritzi
Scenario: Search with valid product code
Given I am in home page
When I search for a valid product code
Then I should see the search results

Scenario: Search with invalid product code
Given I am in home page
When I search for an invalid product code
Then I should see the no results found page

Scenario: Search with numbers
Given I am in home page
When I search for numbers
Then I should see the no results found page

Scenario: Search with special characters
Given I am in home page
When I search for special characters
Then I should see the no results found page

Scenario: Search with all capital valid data
Given I am in home page
When I search for all capital valid data
Then I should see the search results

Scenario: Search with alphanumeric numbers
Given I am in home page
When I search for alphanumeric numbers
Then I should see the no results found page

Scenario: Search with brand name
Given I am in home page
When I search with the brand name
Then I should see the search results

Scenario: Search with catagory
Given I am in home page
When I search with catagory
Then I should see the search results

Scenario: Search with sub catagory
Given I am in home page
When I search for sub catagory
Then I should see the search results


